# Desy Sync&Share

The data reported here contains:

- 'users_all.csv','users_aai.csv': number of registered users:
  - all users
  - non DESY users with log in via Helmholtz AAI
- 'consumption.csv': consumed storage - number of files, stored data in MB:
  - all users
  - non DESY users with log in via Helmholtz AAI

update interval: monthly

Extraction of data from internal monitoring, and pushing of the extracted/filtered data is done manually so far.
